package com.hcloud.common.core.exception;

/**
 * 统一业务层异常，抛出后由advice统一拦截并处理
 * @Auther hepangui
 * @Date 2018/11/2
 */
public class ServiceException extends RuntimeException{
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }
}
